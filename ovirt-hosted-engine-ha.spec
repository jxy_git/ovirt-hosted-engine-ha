%global         package_version 2.4.5
%global         package_name ovirt-hosted-engine-ha
%global         engine_ha_bindir  /usr/share/ovirt-hosted-engine-ha
%global         engine_ha_confdir /etc/ovirt-hosted-engine-ha
%global         engine_ha_logdir  /var/log/ovirt-hosted-engine-ha
%global         engine_ha_rundir  /var/run/ovirt-hosted-engine-ha
%global         engine_ha_statedir /var/lib/ovirt-hosted-engine-ha
%global         vdsm_user vdsm
%global         vdsm_group kvm
%global         vdsm_version 4.40.0
%global         python_interpreter %{__python3}
%global         python_target_version python3
%global         python_sitelib %{python3_sitelib}
%global         engine_ha_libdir  %{python_sitelib}/ovirt_hosted_engine_ha

Summary:        oVirt Hosted Engine High Availability Manager
Name:           ovirt-hosted-engine-ha
Version:        2.4.5
Release:        2
License:        LGPLv2+
URL:            http://www.ovirt.org
Source:         http://resources.ovirt.org/pub/src/%{name}/%{name}-%{package_version}.tar.gz

BuildArch:      noarch

Requires:       python3
Requires:       %{python_target_version}-six
Requires:       %{python_target_version}-otopi >= 1.9.0
Requires:       %{python_target_version}-sanlock >= 3.7.3
Requires:       %{python_target_version}-lxml

BuildRequires:  python3
BuildRequires:  python3-devel
BuildRequires:  python3-mock
BuildRequires:  python3-lxml
BuildRequires:  python3-pytest
BuildRequires:  vdsm-client >= %{vdsm_version}
BuildRequires:  vdsm-python >= %{vdsm_version}
BuildRequires:  vdsm-jsonrpc >= %{vdsm_version}
%{?systemd_requires}
BuildRequires:  systemd

Requires:       bind-utils
Requires:       nmap-ncat
Requires:       sanlock >= 3.7.3
Requires:       sudo
Requires(pre):  vdsm >= %{vdsm_version}
Requires(post): vdsm >= %{vdsm_version}
Requires:       vdsm >= %{vdsm_version}
Requires:       vdsm-client >= %{vdsm_version}
Requires:       vdsm-python >= %{vdsm_version}
Requires:       python3
Requires:       %{python_target_version}-six
Requires:       %{python_target_version}-otopi >= 1.9.0
Requires:       %{python_target_version}-sanlock >= 3.7.3
Requires:       %{python_target_version}-lxml
Conflicts:      ovirt-hosted-engine-setup < 2.4

%description
Hosted engine manager for oVirt project.

%prep
%setup -q -n %{name}-%{package_version}

%build
export PYTHON=python3
PYTHON=%{python_interpreter}
%configure \
        --docdir="%{_docdir}/%{name}-%{version}" \
        --disable-python-syntax-check \
        %{?conf}
%make_build

%check
/usr/bin/pytest

%install
make %{?_smp_mflags} install DESTDIR="%{buildroot}"

install -dDm 0700 %{buildroot}%{engine_ha_logdir}
install -dDm 0700 %{buildroot}%{engine_ha_rundir}
install -dDm 0700 %{buildroot}%{engine_ha_statedir}
install -Dm 0644 initscripts/ovirt-ha-agent.service %{buildroot}%{_unitdir}/ovirt-ha-agent.service
install -Dm 0644 initscripts/ovirt-ha-broker.service %{buildroot}%{_unitdir}/ovirt-ha-broker.service
install -dDm 0750 %{buildroot}%{_sysconfdir}/sudoers.d
install -Dm 0440 sudoers/sudoers %{buildroot}%{_sysconfdir}/sudoers.d/60_ovirt-ha

%files
%license COPYING
%doc README
%doc doc/*.html doc/*.js

%dir %{engine_ha_confdir}
%config(noreplace) %{engine_ha_confdir}/agent-log.conf
%config(noreplace) %{engine_ha_confdir}/agent.conf
%config(noreplace) %{engine_ha_confdir}/broker-log.conf

%dir %{engine_ha_confdir}/notifications
%config(noreplace) %{engine_ha_confdir}/notifications/*

%dir %{engine_ha_bindir}
%{engine_ha_bindir}/ovirt-ha-agent
%{engine_ha_bindir}/ovirt-ha-broker

%dir %{engine_ha_libdir}
%{engine_ha_libdir}/*
%{_unitdir}/ovirt-ha-agent.service
%{_unitdir}/ovirt-ha-broker.service

%config(noreplace) %{_sysconfdir}/sudoers.d/60_ovirt-ha

%defattr(-, %{vdsm_user}, %{vdsm_group}, -)
%dir %{engine_ha_logdir}
%ghost %dir %{engine_ha_rundir}

%dir %{engine_ha_statedir}
%config(noreplace) %{engine_ha_statedir}/ha.conf
%config(noreplace) %{engine_ha_statedir}/broker.conf

%defattr(640, %{vdsm_user}, %{vdsm_group}, -)
%ghost %{engine_ha_logdir}/agent.log
%ghost %{engine_ha_logdir}/broker.log

%post
if [ -e %{_sysconfdir}/ovirt-hosted-engine/answers.conf ] ; then
    chown root:kvm %{_sysconfdir}/ovirt-hosted-engine/answers.conf
fi
# Copy previous broker.conf on upgrade path
if [ $1 -gt 1 ] ; then
    if [ -e "%{engine_ha_confdir}/broker.conf" ]; then
            cp "%{engine_ha_confdir}/broker.conf" "%{engine_ha_statedir}/broker.conf"
            chown %{vdsm_user}:%{vdsm_group}  "%{engine_ha_statedir}/broker.conf"
    fi
fi
%systemd_post ovirt-ha-agent.service
%systemd_post ovirt-ha-broker.service
if [ "$1" -eq 1 ] ; then
#We don't want the service to be started by default before the system
#is configured and Hosted Engine VM deployed
    /usr/bin/systemctl --no-reload disable ovirt-ha-agent.service
    /usr/bin/systemctl --no-reload disable ovirt-ha-broker.service
fi
# Fix logfile ownership for upgrade purposes
if [ -e %{engine_ha_logdir}/agent.log ] ; then
    chown %{vdsm_user}:%{vdsm_group} %{engine_ha_logdir}/agent.log
fi
if [ -e %{engine_ha_logdir}/broker.log ] ; then
    chown %{vdsm_user}:%{vdsm_group} %{engine_ha_logdir}/broker.log
fi

%preun
%systemd_preun ovirt-ha-agent.service
%systemd_preun ovirt-ha-broker.service

%postun
%systemd_postun_with_restart ovirt-ha-agent.service
%systemd_postun_with_restart ovirt-ha-broker.service

%changelog
* Mon Jun 27 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 2.4.5-2
- Change requires from python3-nose to python3-pytest

* Wed Sep 15 2021 wutao <wutao61@huawei.com> - 2.4.5-1
- Package init
